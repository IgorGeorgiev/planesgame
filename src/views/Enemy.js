/**
 * Created by Igor Georgiev on 21-Feb-17.
 */
import * as Constants from '../data/Constants.js'
import {TweenLite} from 'gsap';
class Enemy extends Phaser.Group
{
    constructor(game, instances)
    {
        super(game, game.world);
        this.game = game;

        this.createMultiple(instances, Constants.mainAtlas, 'enemyShip.png');
        game.physics.arcade.enable(this, false);

        this.forEach(function (enemy)
        {
            enemy.damageAmount = 20;
            enemy.body.setSize(enemy.width * 0.5, enemy.height * 0.6);
        }, this);

        this.setAll('anchor.x', 0.5);
        this.setAll('anchor.y', 0.5);
        this.setAll('scale.x', 0.5);
        this.setAll('scale.y', 0.5);
        this.setAll('angle', 180);

        game.world.add(this);
        this.launchEnemy();
    }


    launchEnemy()
    {
        var MIN_ENEMY_SPACING = 0.3;
        var MAX_ENEMY_SPACING = 3;
        var ENEMY_SPEED = 300;

        var enemy = this.getFirstExists(false);

        if (enemy)
        {

            enemy.reset(this.game.rnd.integerInRange(0, this.game.width), -20);
            enemy.body.velocity.x = this.game.rnd.integerInRange(-300, 300);
            enemy.body.velocity.y = ENEMY_SPEED;
            enemy.body.drag.x = 100;
            enemy.update = function()
            {
                enemy.angle = 180 - this.game.math.radToDeg(Math.atan2(enemy.body.velocity.x, enemy.body.velocity.y));
            }
        }

        //  Add enemy after delay
        TweenLite.delayedCall(this.game.rnd.integerInRange(MIN_ENEMY_SPACING, MAX_ENEMY_SPACING), this.launchEnemy, [], this);
    }
}
export default Enemy;