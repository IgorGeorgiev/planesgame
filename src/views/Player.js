/**
 * Created by Igor Georgiev on 16-Feb-17.
 */
import Layout from '../controllers/Layout.js';
import * as Constants from '../data/Constants.js';

class Player extends Phaser.Sprite
{
    constructor(game, x,y, sheet, key)
    {
        super(game, x, y, sheet, key);
        game.physics.arcade.enable(this, false);
        this.health = 100;
        this.anchor.setTo(0.5, 0.5);
        this.game = game;

        this.hitAnim = game.add.image(this.x, this.y, Constants.mainAtlas, 'laserRedShot.png');
        this.hitAnim.anchor.setTo(0.5, 0.5);
        Layout.propHeightScale(this.hitAnim, this, 150);
        this.hitAnim.visible = false;
        this.addChild(this.hitAnim);

        game.world.add(this);
        this.layout();

        this.ACCELERATION_RATE = game.world.height * 0.6;
        this.DRAG = game.world.height * 0.4;
        this.MAX_SPEED = game.world.height * 0.4;
        this.BULLET_SPEED = game.world.height * 0.4;
        this.BULLET_SPACING = game.world.height * 0.25;

        this.body.maxVelocity.setTo(this.MAX_SPEED, this.MAX_SPEED);
        this.body.drag.setTo(this.DRAG, this.DRAG);

        this.velocity = this.body.velocity;
        this.acceleration = this.body.acceleration;
        this.shipExhaust();

        this.minMouseDist = game.world.height * 0.2;
        this.bulletTimer = 0;
        this.addBullets();

    }

    layout()
    {
        Layout.horizontalAlignCenter(this, this.game.world);
        Layout.verticalAlignBottom(this, this.game.world, 10);
    }

    shipExhaust()
    {
        this.trail = this.game.add.emitter(0, 0, 50);
        Layout.verticalAlignBottom(this.trail, this, 50);

        this.trail.width = this.height * 0.1;
        this.trail.makeParticles([Constants.mainAtlas], 'laserGreen.png');
        this.trail.setXSpeed(30, -30);
        this.trail.setYSpeed(200, 180);
        this.trail.setRotation(50,-50);
        this.trail.setAlpha(1, 0.01, 3000);
        this.trail.setScale(0.05, 0.4, 0.05, 0.4, 2000, Phaser.Easing.Quintic.Out);
        this.trail.start(false, 2500, 42);

        this.events.onKilled.add(this.trail.kill, this.trail);
    }

    alignExhaustTrail()
    {
        this.trail.x = this.x;
    }

    addBullets()
    {
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(20, Constants.mainAtlas, 'laserGreen.png');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);

    }

    fireBullet()
    {
        if(this.game.time.now > this.bulletTimer)
        {
            let bullet = this.bullets.getFirstExists(false);

            if (bullet)
            {
                let bulletOffset = 20 * Math.sin(this.game.math.degToRad(this.angle));
                bullet.reset(this.x + bulletOffset, this.y);
                bullet.angle = this.angle;
                this.game.physics.arcade.velocityFromAngle(bullet.angle - 90, this.BULLET_SPEED, bullet.body.velocity);
                bullet.body.velocity.x += this.velocity.x;

                this.bulletTimer = this.game.time.now + this.BULLET_SPACING;
            }
        }

    }

    takeHit()
    {
        this.hitAnim.visible = true;
        this.hitAnim.alpha = 1;

        TweenLite.to(this.hitAnim, 0.3, {alpha:0, onComplete:function () {
            this.hitAnim.visible = false;
        }, onCompleteScope:this});
    }
}
export default Player;