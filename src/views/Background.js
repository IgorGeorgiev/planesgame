/**
 * Created by Igor Georgiev on 01-Mar-17.
 */

import * as Constants from '../data/Constants.js';
import Layout from '../controllers/Layout.js';

class Background extends Phaser.Tilemap
{
    constructor(game, mapKey, tileWidth, tileHeight)
    {
        super(game, mapKey, tileWidth, tileHeight);

        this.addTilesetImage('tileset', 'tileset', tileWidth, tileHeight);
        this.setCollisionBetween(0, 9);
        this.mapLayer = this.createLayer(0);


        game.physics.arcade.enable(this.mapLayer, true, true);


        //var g = new Phaser.Group(game, undefined, undefined, g);

        let requiredScale = this.game.world.width / this.widthInPixels;
        this.mapLayer.scale.setTo(requiredScale);
        this.mapLayer.
        game.world.add(this.mapLayer);


    }

    hittest()
    {
        console.log('hit');
    }
}
export default Background;