/**
 * Created by Igor Georgiev on 20-Feb-17.
 */

export default class WindowDimensions
{
    static deviceScreenAspect()
    {
        let deviceAspect = 0;

        if(window.screen.width > window.screen.height)
        {
            deviceAspect = window.screen.width/window.screen.height;
        }
        else
        {
            deviceAspect = window.screen.height/window.screen.width;

        }

        return deviceAspect;
    }

    static availableWindowAspect()
    {
        let deviceAspect = 0;

        if(window.innerWidth > window.innerHeight)
        {
            deviceAspect = window.innerWidth/window.innerHeight;
        }
        else
        {
            deviceAspect = window.innerHeight/window.innerWidth;

        }

        return deviceAspect;
    }

};