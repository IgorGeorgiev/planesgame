/**
 * Created by Igor Georgiev on 14-Feb-17.
 */

class Preload extends Phaser.State
{
    preload()
    {
        this.load.atlas('mainSheet', 'assets/img/mainSheet.png', 'assets/img/mainSheet.json');
        this.load.tilemap('Map', 'assets/img/map.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.image('tileset', 'assets/img/tileset.png');
    }

    create()
    {
        this.state.start('GameState');

    }
}
export default Preload;