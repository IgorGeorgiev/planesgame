/**
 * Created by Igor Georgiev on 14-Feb-17.
 */
class Boot extends Phaser.State {

    create()
    {
        // Configure additional rendering properties
        this.renderer.renderSession.roundPixels = false;
        this.stage.disableVisibilityChange = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.forceOrientation(false, true);
        this.scale.refresh();
        this.state.start('Preload');
    }

}

export default Boot;