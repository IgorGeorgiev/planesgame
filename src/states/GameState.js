import Player from "../views/Player.js";
import Enemy from "../views/Enemy.js";
import Layout from '../controllers/Layout.js';
import BackgroundModel from '../models/BackgroundModel.js';
import Background from '../views/Background.js';

import * as Constants from '../data/Constants.js';
//import RainbowText from 'views/RainbowText';
class GameState extends Phaser.State {

	create() {
        this.debug = true;
        this.game.stage.backgroundColor = '#FF0000';
		// window.onresize = this.onResize.bind(this);
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        // this.game.cache.addTilemap('testMap', null, BackgroundModel.createData(this.game), Phaser.Tilemap.CSV);
        this.background = new Background(this.game, 'Map', 64, 64);

/*        var i = new Phaser.Sprite(this.game, 0, 0, Constants.mainAtlas, 'water.png');
        Layout.propWidthScale(i, this.game.world, 99);
        i.x = Layout.relWidth(.5, this.game.world);
        this.game.world.add(i);*/

        console.log(BackgroundModel.createData(this.game));
		this.player = new Player(this.game, 0,0, Constants.mainAtlas, 'player.png');
        this.player.inputEnabled = true;
        // this.player.events.onInputDown.add(this.fullscreen, this);
        this.inertia = 0;
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.fireButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.score = 0;
		// var topLeft = this.game.add.image(0, 0, 'mainSheet', 'laserGreen.png');
		// this.testArray.push(topLeft);
		// var topRight = this.game.add.image(this.game.world.width - topLeft.width, 0, 'mainSheet', 'laserGreen.png');
		// this.testArray.push(topRight);
		// var bottomLeft = this.game.add.image(0, this.game.world.height - topRight.height, 'mainSheet', 'laserGreen.png');
		// this.testArray.push(bottomLeft);'
		// var bottomRight = this.game.add.image(this.game.world.width - topLeft.width, this.game.world.height - topRight.height, 'mainSheet', 'laserGreen.png');
		// this.testArray.push(bottomRight);
        // this.layout();

		console.log(this.game.world.width + 'X' + this.game.world.height);
		console.log(document.documentElement.clientWidth + "X" + document.documentElement.clientHeight + " document elements");
        console.log(window.screen.width + "X" + window.screen.height + " window.screen");

        // this.enemy = new Enemy(this.game, 50);

        this.healthStat = this.game.add.text(0,0, '', { font: '20px Arial', fill: '#f00' });
        this.healthStat.render = this.renderHealth();

        this.scoreTxt = this.game.add.text(0,0, '', { font: '20px Arial', fill: '#fff' });
        this.scoreTxt.render = this.renderScore();

            //  Game over text
        this.gameOver = this.game.add.text(this.game.world.centerX, this.game.world.centerY, 'GAME OVER!', { font: '84px Arial', fill: '#fff' });
        this.gameOver.anchor.setTo(0.5, 0.5);
        this.gameOver.visible = false;
    }

    renderScore()
    {
        this.scoreTxt.text = 'Score: ' + this.score;
        Layout.horizontalAlignRight(this.scoreTxt, this.game.world, 0);
    }
    renderHealth()
    {
        this.healthStat.text = 'Health: ' + Math.max(this.player.health, 0) +' hp';
    }

	fullscreen()
	{

        if (this.game.scale.isFullScreen)
		{
			this.game.scale.stopFullScreen();
		}
		else
		{
            this.game.scale.startFullScreen();
        }
	}

    handleShipCollision(player, enemy)
    {
        enemy.kill();
        player.takeHit();
        player.damage(enemy.damageAmount);
        this.renderHealth();
    }

    hitEnemyWithBullet(enemy, bullet)
    {
        enemy.kill();
        bullet.kill();
        this.score += enemy.damageAmount * 10;
        this.renderScore();
    }

	onResize()
	{
		// let aspect = window.screen.width/window.screen.height;
        //
		// let height = window.innerHeight;
		// let width = height / aspect;
        //
		// this.game.scale.setGameSize(width, height);
		// // this.scale.scaleMode = Phaser.ScaleManager.RESIZE;
		// this.game.scale.refresh();

        // layout();
	}

	restartGame()
    {
        this.enemy.bullets.callAll('kill', this.enemy.bullets);
        this.enemy.callAll('kill', this.enemy);

        this.player.revive(100);
        this.renderHealth();
        this.score = 0;
        this.renderScore();
        this.gameOver.visible = false;
    }

	update()
	{
        this.player.acceleration.x = 0;

        //Movement

        //Keyboard
		if(this.cursors.left.isDown)
		{
            this.player.acceleration.x = this.player.ACCELERATION_RATE * -1;
		}
		else if(this.cursors.right.isDown)
		{
            this.player.acceleration.x = this.player.ACCELERATION_RATE;

        }

        //Mouse
        if (
            this.game.input.x < this.game.width - 20 &&
            this.game.input.x > 20 &&
            this.game.input.y > 20 &&
            this.game.input.y < this.game.height - 20)
        {
            let dist = this.game.input.x - this.player.x;
            this.player.velocity.x = this.player.MAX_SPEED * this.game.math.clamp(dist / this.player.minMouseDist, -1, 1);
            this.player.y = this.game.input.y;
        }

        //Stop ot borders
        if(this.player.x > this.game.world.width - this.player.width * 0.5)
        {
            this.player.x = this.game.world.width - this.player.width * 0.5;
            this.player.acceleration.x = 0;
        }

        if(this.player.x < this.player.width * 0.5)
        {
            this.player.x = this.player.width * 0.5;
            this.player.acceleration.x = 0;
        }

        //Inertia effect
        this.inertia = this.player.velocity.x / this.player.MAX_SPEED;
        this.player.scale.x = 1 - Math.abs(this.inertia) * 0.3;
        this.player.angle = this.inertia * 12;
        //Ship's exhaust
        this.player.alignExhaustTrail();

        //Fire
        if (this.player.alive && (this.fireButton.isDown || this.game.input.activePointer.isDown))
        {
            this.player.fireBullet();
        }

        //Check for collision between ships
        // if(this.enemy)
        // {
        //     this.enemy.forEach(function (element)
        //     {
        //         if(element.inWorld == true)
        //         {
        //             element.outOfBoundsKill = true;
        //         }
        //     }, this.enemy)
        // }
        // this.game.physics.arcade.overlap(this.player, this.enemy, this.handleShipCollision, null, this);
        // this.game.physics.arcade.overlap(this.enemy, this.player.bullets, this.hitEnemyWithBullet, null, this);

        //Game over check
        if(!this.player.alive && this.gameOver.visible == false)
        {
            this.gameOver.visible = true;
            let tapRestart = this.game.input.onTap.addOnce(_restart,this);
            let spaceRestart = this.fireButton.onDown.addOnce(_restart,this);

            function _restart()
            {
                tapRestart.detach();
                spaceRestart.detach();
                this.restartGame();
            }
        }
        this.game.physics.arcade.collide(this.player, this.background.mapLayer.body);
    }

    render()
    {

        if(this.debug == true)
        {

            // for (let i = 0; i < this.enemy.length; i++)
            // {
            //     this.game.debug.body(this.enemy.children[i]);
            // }

            for (let i = 0; i < this.player.bullets.length; i++)
            {
                this.game.debug.body(this.player.bullets.children[i]);
            }


            this.game.debug.body(this.player);

            this.game.debug.body(this.background);
        }

    }
}

export default GameState;
