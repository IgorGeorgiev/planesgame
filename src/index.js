import Boot from 'states/Boot';
import Preload from 'states/Preload';
import GameState from 'states/GameState';
import * as Constants from 'data/Constants.js'
// var TweenMax = require('D:/HTML/planes/node_modules/gsap/TweenMax.js');

class Game extends Phaser.Game {

	constructor() {


		let height = "100%";
		let width = window.screen.height / Constants.SIXTEEN_TO_NINE;

		super(width, height, Phaser.AUTO, 'content', null);
		this.state.add('Boot', Boot, false);
		this.state.add('Boot', Preload, false);
		this.state.add('GameState', GameState, false);
		this.state.start('Boot');
	};

}
new Game();
