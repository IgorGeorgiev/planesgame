/**
 * Created by Igor Georgiev on 01-Mar-17.
 */
export default class BackgroundModel
{
    static createData(game)
    {
        let data = '';

        for (let y = 0; y < 10; y++)
        {
            for (let x = 0; x < 10; x++)
            {
                data += game.rnd.between(0, 1).toString();

                if (x < 9)
                {
                    data += ',';
                }
            }

            if (y < 9)
            {
                data += "\n";
            }
        }

        data = '0,-1,0,-1,0,-1,0;';
        return data;
    }

}