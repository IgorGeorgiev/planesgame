/**
 * @class 	Layout
 * @author	Wiener Games
 * @description
 * Layout utility functions
 * @param {Phaser.Game} game - Reference to the Phaser Game instance.
 */

export default class Layout
{
    /**
     * @method localToGlobal
     * @public
     * @memberof Layout
     * @description
     * Converts point from local coordinates of a display object to global coordinates
     * Does not work if any object in the hierarchy has rotation applied
     * For example , a point (50,50) in a container which is at world coordinates (100,100) will be converted to (150,150)
     * @param displayObject
     * @param point
     */
    static localToGlobal(displayObject, point)
    {
        var child = displayObject;
        var parent = displayObject.parent;

        while(parent != undefined)
        {
            point.x = child.x + point.x * child.scale.x;
            point.y = child.y + point.y * child.scale.y;
            child = parent;
            parent = parent.parent;
        }

        return point;
    };


    /**
     * @method globalToLocal
     * @public
     * @memberof Layout
     * @description
     * Converts point from global coordinates to local coordinates of a display object
     * Does not work if any object in the hierarchy has rotation applied
     * @param displayObject
     * @param point
     */
    static globalToLocal(displayObject, point)
    {
        var displayObjects = [];

        var child = displayObject;
        var world = displayObject.game.world;

        while(child != world)
        {
            displayObjects.push(child);
            child = child.parent;
        }

        while(displayObjects.length > 0)
        {
            child = displayObjects.shift();
            point.x = (point.x - child.x) / child.scale.x;
            point.y = (point.y - child.y) / child.scale.y;
        }

        return point;
    };


    /**
     * @method - centerX
     * @public
     * @description
     * Calculates an X position value that would center a display object within another.
     * @param {number} firstX - X position of target object.
     * @param {number} firstWidth - Width of target object.
     * @param {number} secondWidth - Width of object to reposition.
     * @return {number} - new X position for object to reposition.
     */
    static centerX(firstX, firstWidth, secondWidth)
    {
        return (firstX + firstWidth * 0.5) - secondWidth * 0.5;
    };

    /**
     * @method - centerY
     * @public
     * @description
     * Calculates a Y position value that would center a display object within another.
     * @param {number} firstY - Y position of target object.
     * @param {number} firstHeight - Height of target object.
     * @param {number} secondHeight - Height of object to reposition.
     * @return {number} - new Y position for object to reposition.
     */
    static centerY(firstY, firstHeight, secondHeight)
    {
        return (firstY + firstHeight * 0.5) - secondHeight * 0.5;
    };


    /**
     * @method propWidthScale
     * @public
     * @memberof Layout
     * @description
     * Scales an object proportionally (preserving aspect)
     * @param target - object to be scaled
     * @param src - the object we compare to
     * @param sizePercent - percent of source size that the target should be
     */
    static propWidthScale(target, src, sizePercent)
    {
        var scaleNeeded = (Math.abs(src.width) * sizePercent / 100) / Math.abs(target.width);
        target.scale.x = scaleNeeded;
        target.scale.y = scaleNeeded;
    };


    /**
     * @method propHeightScale
     * @public
     * @memberof Layout
     * @description
     * Scales an object proportionally (preserving aspect)
     * @param target - object to be scaled
     * @param src - the object we compare to
     * @param sizePercent - percent of source size that the target should be
     */
    static propHeightScale(target, src, sizePercent)
    {
        var scaleNeeded = (Math.abs(src.height) * sizePercent / 100) / Math.abs(target.height);
        target.scale.x = scaleNeeded;
        target.scale.y = scaleNeeded;
    };


    /**
     * @method horizontalAlignLeft
     * @public
     * @memberof Layout
     * @description
     * Aligns an object to another
     * @param target - aligned object
     * @param src - object we align to
     * @param offsetX - percent offset from the source left border
     */
    static horizontalAlignLeft(target, src, offsetX)
    {
        target.x = Math.round(src.x + Math.abs(src.width) * offsetX / 100);
    };


    /**
     * @method horizontalAlignRight
     * @public
     * @memberof Layout
     * @description
     * Aligns an object to another
     * @param target - aligned object
     * @param src - object we align to
     * @param offsetX - percent offset from the source right border
     */
    static horizontalAlignRight(target, src, offsetX)
    {
        target.x = Math.round(src.x + Math.abs(src.width) * (100 - offsetX) / 100 - Math.abs(target.width));
    };


    /**
     * @method horizontalAlignCenter
     * @public
     * @memberof Layout
     * @description
     * Aligns an object to another
     * @param target - aligned object
     * @param src - object we align to
     * @param offsetX - percent offset from the source center
     */
    static horizontalAlignCenter(target, src, offsetX)
    {
        offsetX = offsetX == undefined ? 0 : offsetX;

        target.x = Math.round(src.x + Math.abs(src.width) * (50 + offsetX) / 100 - Math.abs(target.width) * .5);
    };


    /**
     * @method verticalAlignTop
     * @public
     * @memberof Layout
     * @description
     * Aligns an object to another
     * @param target - aligned object
     * @param src - object we align to
     * @param offsetY - percent offset from the source top
     */
    static verticalAlignTop(target, src, offsetY)
    {
        target.y = Math.round(src.y + Math.abs(src.height) * offsetY / 100);
    };


    /**
     * @method verticalAlignMiddle
     * @public
     * @memberof Layout
     * @description
     * Aligns an object to another
     * @param target - aligned object
     * @param src - object we align to
     * @param offsetY - percent offset from the source middle
     */
    static verticalAlignMiddle(target, src, offsetY)
    {
        offsetY = offsetY == undefined ? 0 : offsetY;

        target.y = Math.round(src.y + Math.abs(src.height) * (50 + offsetY) / 100 - Math.abs(target.height) * .5);
    };


    /**
     * @method verticalAlignBottom
     * @public
     * @memberof Layout
     * @description
     * Aligns an object to another
     * @param target - aligned object
     * @param src - object we align to
     * @param offsetY - percent offset from the source bottom
     */
    static verticalAlignBottom(target, src, offsetY)
    {
        target.y = Math.round(src.y + Math.abs(src.height) - Math.abs(target.height) - Math.abs(src.height) * offsetY / 100);
    };


    /**
     * @method setX
     * @public
     * @memberof Layout
     * @description
     * Set the x of an object relative to another
     * @param target - object positioned
     * @param src - object in relation to which we position
     * @param x - the relative x position (percent)
     */
    static setX(target, src, x)
    {
        target.x = src.x + Math.abs(src.width) * x / 100;
    };


    /**
     * @method setY
     * @public
     * @memberof Layout
     * @description
     * Set the y of an object relative to another
     * @param target - object positioned
     * @param src - object in relation to which we position
     * @param y - the relative y position (percent)
     */
    static setY(target, src, y)
    {
        target.y = src.y + Math.abs(src.height) * y / 100;
    };


    /**
     * @method relWidth
     * @public
     * @memberof Layout
     * @description
     * Calculates a percentage of the width of an object
     * @param w - the width in percent
     * @param src - the object used as a reference
     * @returns {number} the width calculated
     */
    static relWidth(w, src)
    {
        src = src == undefined ? this.game.world : src;
        return src.width * w / 100;
    };


    /**
     * @method relHeight
     * @public
     * @memberof Layout
     * @description
     * Calculates a percentage of the width of an object
     * @param h - the height in percent
     * @param src - the object used as a reference
     * @returns {number} - the height calculated
     */
    static relHeight(h, src)
    {
        src = (src == undefined) ? this.game.world : src;
        return Math.abs(src.height) * h / 100;
    };


};

